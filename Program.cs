﻿#region Merge SortArray
/*
int[] arr1 = {1,2,3,0,0,0};
int[] arr2 = {1,2,3};
Merge(ref arr1,3,  ref arr2 ,3);

foreach (var item in arr1)
{
    Console.WriteLine(item + " ");
}


    void Merge( ref int[] nums1, int m,ref int[] nums2, int n)
    {
        
        int[] resNums = new int[n + m];
        int j = 0;
        for (int i = 0; i < nums1.Length; i++)
        {
            if(nums1 == null)
            {
               break;
            }
            else if (nums1[i]!=0)
            {
               resNums[j++] = nums1[i];
            }
        }

        j = m;
        for (int i = 0; i < nums2.Length; i++)
        {
        if (n == 0)
        {
            break;
        }
        else
          {
            resNums[j++] = nums2[i]; 
          }  
        }

    Array.Sort(resNums);
     nums1 = resNums;
    }
*/
#endregion

using LeetCodeProblems2;
using Newtonsoft.Json;
using System.Text.Json;
using System.Text.Json.Serialization;

Console.Write("from : ");
var from = Console.ReadLine();

Console.Write("to : ");
var to = Console.ReadLine();

while (true)
{
    Console.Write($"Write here in {from}  : ");
    var content = Console.ReadLine();

    var client = new HttpClient();

    var request = new HttpRequestMessage
    {
        Method = HttpMethod.Post,
        RequestUri = new Uri("https://google-translate1.p.rapidapi.com/language/translate/v2"),
        Headers =
    {
        { "X-RapidAPI-Key", "0e2ad1eed1mshb0136a5598b5266p1a7cdbjsncd799d7126ac" },
        { "X-RapidAPI-Host", "google-translate1.p.rapidapi.com" },
    },
        Content = new FormUrlEncodedContent(new Dictionary<string, string>
    {
        { "q", content },
        { "target", to },
        { "source", from },
    }),
    };


    using (var response = await client.SendAsync(request))
    {
        response.EnsureSuccessStatusCode();
        var body = await response.Content.ReadAsStringAsync();
        Console.WriteLine(body);
        var result = JsonConvert.DeserializeObject<ExpectedClass>(body);
        Console.WriteLine(result.Data.Translations[0].Translated);

        
    }
}