﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace LeetCodeProblems2
{
    public class ExpectedClass
    {
        [JsonPropertyName("data")]
        public Data Data { get; set; }
    }
}
