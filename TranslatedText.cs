﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace LeetCodeProblems2
{
    public class TranslatedText
    {
        [JsonPropertyName("translatedText")]
        public string Translated { get; set; }
    }
}
