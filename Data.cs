﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace LeetCodeProblems2
{
    public class Data
    {

        [JsonPropertyName("translations")]
        public List<TranslatedText> Translations;

    }
}
